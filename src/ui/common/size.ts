export enum BaseSize
{
    default = 'h-8',
    small = 'h-6',
    large = 'h-10'
}

export enum Size
{
    default = 'h-8',
    small = 'h-6',
    large = 'h-10'
}

export enum WrapSize
{
    default = 'h-6',
    small = 'h-4',
    large = 'h-8'
}

export enum MinSize
{
    default = 'min-h-8',
    small = 'min-h-6',
    large = 'min-h-10'
}

export enum PtPbSize
{
    default = '0.250rem',
    small = '0.125rem',
    large = '0.400rem'
}

export enum TabSize
{
    default = 'h-8',
    small = 'h-6',
}

export enum SkeletonSize
{
    mini = 'h-4',
    default = BaseSize.default,
    small = BaseSize.small,
    large = BaseSize.large
}

export enum ButtonSize
{
    small = 'h-6 px-3 text-sm',
    default = 'h-8 px-4 text-sm',
    large = 'h-10 px-6 text-base'
}

export enum ButtonRoundedSize
{
    small = 'h-6 w-6',
    default = 'h-8 w-8',
    large = 'h-10 w-10'
}

export enum WrapperSize
{
    default = 'h-8 w-8',
    small = 'h-6 w-6',
    large = 'h-12 w-12'
}

export enum TagSize
{
    default = 'h-6 px-3',
    medium = 'h-8 px-2',
    large = 'h-10 px-4'
}

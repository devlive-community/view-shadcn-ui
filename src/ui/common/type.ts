export enum BaseBackgroundType
{
    primary = 'bg-blue-400',
    success = 'bg-green-400',
    warning = 'bg-yellow-400',
    error = 'bg-red-400'
}

export enum BaseBorderType
{
    primary = 'border-blue-400',
    success = 'border-green-400',
    warning = 'border-yellow-400',
    error = 'border-red-400'
}

export enum BaseTextType
{
    primary = 'text-blue-400',
    success = 'text-green-400',
    warning = 'text-yellow-400',
    error = 'text-red-400'
}

export enum HoverType
{
    primary = 'hover:border-blue-400',
    success = 'hover:border-green-400',
    warning = 'hover:border-yellow-400',
    error = 'hover:border-red-400'
}

export enum HoverTextType
{
    primary = 'hover:text-blue-400',
    success = 'hover:text-green-400',
    warning = 'hover:text-yellow-400',
    error = 'hover:text-red-400'
}

export enum TextType
{
    default = 'text-black',
    primary = 'text-blue-400',
    success = 'text-green-400',
    warning = 'text-yellow-400',
    error = 'text-red-400'
}

export enum BorderType
{
    primary = 'border-blue-400',
    success = 'border-green-400',
    warning = 'border-yellow-400',
    error = 'border-red-400'
}

export enum BorderRightType
{
    primary = 'border-r-blue-400',
    success = 'border-r-green-400',
    warning = 'border-r-yellow-400',
    error = 'border-r-red-400'
}

export enum BackgroundType
{
    primary = 'bg-blue-50',
    success = 'bg-green-50',
    warning = 'bg-yellow-50',
    error = 'bg-red-50'
}

export enum ButtonBackgroundType
{
    default = 'bg-white',
    primary = 'bg-blue-400',
    info = 'bg-blue-400',
    success = 'bg-green-400',
    warning = 'bg-yellow-400',
    error = 'bg-red-400',
    danger = 'bg-red-400',
    text = 'bg-transparent'
}

export enum TagBackgroundType
{
    default = 'bg-gray-200',
    primary = 'bg-blue-400',
    success = 'bg-green-400',
    warning = 'bg-yellow-400',
    error = 'bg-red-400',
}

export enum TagBorderType
{
    default = 'border-gray-300',
    primary = 'border-blue-500',
    success = 'border-green-500',
    warning = 'border-yellow-500',
    error = 'border-red-500',
}

export enum ButtonHoverType
{
    default = 'hover:bg-white',
    primary = 'hover:bg-blue-500',
    info = 'hover:bg-blue-500',
    success = 'hover:bg-green-500',
    warning = 'hover:bg-yellow-500',
    error = 'hover:bg-red-500',
    danger = 'hover:bg-red-500',
    text = 'hover:bg-transparent'
}

export enum SkeletonType
{
    circle,
    square,
    rect,
    image
}

export enum MessageType
{
    info,
    success,
    warning,
    error,
    loading
}

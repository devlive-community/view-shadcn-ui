export enum ArrangePosition
{
    left = 'left',
    right = 'right',
    top = 'top',
    bottom = 'bottom'
}

export enum ArrangeDirection
{
    horizontal,
    vertical
}

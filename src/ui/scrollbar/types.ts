export interface ScrollbarProps
{
    height?: string | number
    position?: 'left' | 'right'
}

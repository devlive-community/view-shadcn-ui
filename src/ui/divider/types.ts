export interface DividerProps
{
    text?: string
    type?: 'horizontal' | 'vertical'
    orientation?: 'left' | 'center' | 'right'
    dashed?: boolean
}

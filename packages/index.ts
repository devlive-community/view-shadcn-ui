import { App } from 'vue'

import Message from './setup/message.ts'
import Spin from './setup/spin.ts'

import { Language, setLocale } from '@/utils/locale.ts'

import ShadcnButton from '@/ui/button'
import ShadcnButtonGroup from '@/ui/button/group'
import ShadcnCard from '@/ui/card'
import { ShadcnCopy } from '@/ui/copy'
import { ShadcnInput } from '@/ui/input'
import ShadcnIcon from '@/ui/icon'
import ShadcnModal from '@/ui/modal'
import ShadcnTooltip from '@/ui/tooltip'
import { ShadcnCodeEditor } from '@/ui/code-editor'
import ShadcnRow from '@/ui/row'
import ShadcnCol from '@/ui/col/ShadcnCol.vue'
import ShadcnDivider from '@/ui/divider'
import ShadcnEllipsis from '@/ui/ellipsis'
import ShadcnAvatar from '@/ui/avatar'
import ShadcnAvatarGroup from '@/ui/avatar/group'
import { ShadcnBadge } from '@/ui/badge'
import ShadcnAlert from '@/ui/alert'
import ShadcnProgress from '@/ui/progress'
import ShadcnSpace from '@/ui/space'
import ShadcnSwitch from '@/ui/switch'
import ShadcnRadio from '@/ui/radio'
import ShadcnCheckbox from '@/ui/checkbox'
import ShadcnCheckboxGroup from '@/ui/checkbox/group'
import ShadcnRadioGroup from '@/ui/radio/group'
import ShadcnSelect from '@/ui/select'
import ShadcnSelectOption from '@/ui/select/option'
import ShadcnSelectGroup from '@/ui/select/group'
import ShadcnRate from '@/ui/rate'
import { ShadcnTab, ShadcnTabItem } from '@/ui/tab'
import { ShadcnSkeleton, ShadcnSkeletonItem } from '@/ui/skeleton'
import ShadcnLayout from '@/ui/layout'
import ShadcnLayoutHeader from '@/ui/layout/header'
import ShadcnLayoutContent from '@/ui/layout/content'
import ShadcnLayoutSider from '@/ui/layout/sider'
import ShadcnLayoutFooter from '@/ui/layout/footer'
import ShadcnLayoutWrapper from '@/ui/layout/wrapper'
import ShadcnLayoutMain from '@/ui/layout/main'
import ShadcnPagination from '@/ui/pagination'
import ShadcnDrawer from '@/ui/drawer'
import ShadcnMenu from '@/ui/menu'
import ShadcnMenuItem from '@/ui/menu/item'
import ShadcnMenuSub from '@/ui/menu/sub'
import ShadcnMenuGroup from '@/ui/menu/group'
import ShadcnMessage from '@/ui/message'
import ShadcnTable from '@/ui/table'
import ShadcnBreadcrumb from '@/ui/breadcrumb'
import ShadcnBreadcrumbItem from '@/ui/breadcrumb/item'
import { ShadcnSlider } from '@/ui/slider'
import ShadcnSpin from '@/ui/spin'
import ShadcnForm from '@/ui/form'
import ShadcnFormItem from '@/ui/form/item'
import ShadcnTimeline from '@/ui/timeline'
import ShadcnTimelineItem from '@/ui/timeline/item'
import ShadcnTrend from '@/ui/trend'
import ShadcnException from '@/ui/exception'
import ShadcnLink from '@/ui/link'
import ShadcnCollapse from '@/ui/collapse'
import ShadcnCollapseItem from '@/ui/collapse/item'
import ShadcnTag from '@/ui/tag'
import ShadcnGlobalFooter from '@/ui/footer/global'
import ShadcnToolbarFooter from '@/ui/footer/toolbar'
import ShadcnWatermark from '@/ui/watermark'
import ShadcnHighlight from '@/ui/highlight'
import ShadcnDropdown from '@/ui/dropdown'
import ShadcnDropdownItem from '@/ui/dropdown/item'
import ShadcnNumber from '@/ui/number'
import ShadcnTree from '@/ui/tree'
import ShadcnText from '@/ui/text'
import ShadcnGradientText from '@/ui/text/gradient'
import { ShadcnContextMenu, ShadcnContextMenuItem, ShadcnContextMenuSub } from '@/ui/contextmenu'
import ShadcnHoverCard from '@/ui/hover-card'
import { ShadcnToggle, ShadcnToggleGroup } from '@/ui/toggle'
import { ShadcnDataBuilderCanvas, ShadcnDataBuilderConfigure, ShadcnDataBuilderEditor, ShadcnDataBuilderPanel, ShadcnDataBuilderView } from '@/ui/data-builder'
import { ShadcnUpload } from '@/ui/upload'
import { ShadcnLogger } from '@/ui/logger'
import { ShadcnScrollbar } from '@/ui/scrollbar'
import { ShadcnCountDown } from '@/ui/count-down'
import { ShadcnEmpty } from '@/ui/empty'
import { ShadcnBackTop } from '@/ui/back-top'
import { ShadcnWorkflowEditor, ShadcnWorkflowView } from '@/ui/workflow'
import { ShadcnInputTag } from '@/ui/input-tag'
import { ShadcnMap } from '@/ui/map'
import { ShadcnLoadingBar } from '@/ui/loading-bar'
import { ShadcnFloatButton } from '@/ui/float-button'
import { ShadcnDataFilter, ShadcnHierarchicalDataFilter } from '@/ui/data-filter'
import ShadcnQrCode from '@/ui/qrcode/ShadcnQrCode.vue'
import { ShadcnTimePicker } from '@/ui/time-picker'
import { ShadcnColorPicker } from '@/ui/color-picker'
import { ShadcnDatePicker } from '@/ui/date-picker'
import { ShadcnCron } from '@/ui/cron'
import { ShadcnContribution } from '@/ui/contribution'
import { ShadcnMention } from '@/ui/mention'
import { ShadcnImage, ShadcnImageGroup, ShadcnImageViewer } from '@/ui/image'
import { ShadcnCarousel } from '@/ui/carousel'
import { ShadcnDataTable, TableCellInputEditor, TableCellSelectEditor } from '@/ui/data-table'
import { ShadcnMarquee } from '@/ui/marquee'

let components = [
    ShadcnButton,
    ShadcnButtonGroup,
    ShadcnCard,
    ShadcnCopy,
    ShadcnCodeEditor,
    ShadcnInput,
    ShadcnIcon,
    ShadcnModal,
    ShadcnTooltip,
    ShadcnRow,
    ShadcnCol,
    ShadcnDivider,
    ShadcnEllipsis,
    ShadcnAvatar,
    ShadcnAvatarGroup,
    ShadcnBadge,
    ShadcnAlert,
    ShadcnProgress,
    ShadcnSpace,
    ShadcnSwitch,
    ShadcnRadio,
    ShadcnRadioGroup,
    ShadcnCheckbox,
    ShadcnCheckboxGroup,
    ShadcnSelect,
    ShadcnSelectOption,
    ShadcnSelectGroup,
    ShadcnRate,
    ShadcnTab, ShadcnTabItem,
    ShadcnSkeleton, ShadcnSkeletonItem,
    ShadcnLayout,
    ShadcnLayoutHeader,
    ShadcnLayoutContent,
    ShadcnLayoutSider,
    ShadcnLayoutFooter,
    ShadcnLayoutWrapper,
    ShadcnLayoutMain,
    ShadcnPagination,
    ShadcnDrawer,
    ShadcnMenu,
    ShadcnMenuItem,
    ShadcnMenuSub,
    ShadcnMenuGroup,
    ShadcnMessage,
    ShadcnTable,
    ShadcnBreadcrumb,
    ShadcnBreadcrumbItem,
    ShadcnSlider,
    ShadcnSpin,
    ShadcnForm,
    ShadcnFormItem,
    ShadcnTimeline,
    ShadcnTimelineItem,
    ShadcnTrend,
    ShadcnException,
    ShadcnLink,
    ShadcnCollapse,
    ShadcnCollapseItem,
    ShadcnTag,
    ShadcnGlobalFooter,
    ShadcnToolbarFooter,
    ShadcnWatermark,
    ShadcnHighlight,
    ShadcnDropdown,
    ShadcnDropdownItem,
    ShadcnNumber,
    ShadcnTree,
    ShadcnText,
    ShadcnGradientText,
    ShadcnContextMenu, ShadcnContextMenuItem, ShadcnContextMenuSub,
    ShadcnHoverCard,
    ShadcnToggle, ShadcnToggleGroup,
    ShadcnDataBuilderPanel, ShadcnDataBuilderCanvas, ShadcnDataBuilderConfigure, ShadcnDataBuilderEditor, ShadcnDataBuilderView,
    ShadcnUpload,
    ShadcnLogger,
    ShadcnScrollbar,
    ShadcnCountDown,
    ShadcnEmpty,
    ShadcnBackTop,
    ShadcnWorkflowEditor, ShadcnWorkflowView,
    ShadcnInputTag,
    ShadcnMap,
    ShadcnLoadingBar,
    ShadcnFloatButton,
    ShadcnDataFilter, ShadcnHierarchicalDataFilter,
    ShadcnQrCode,
    ShadcnTimePicker,
    ShadcnColorPicker,
    ShadcnDatePicker,
    ShadcnCron,
    ShadcnContribution,
    ShadcnMention,
    ShadcnImage, ShadcnImageGroup, ShadcnImageViewer,
    ShadcnCarousel,
    ShadcnDataTable, TableCellInputEditor, TableCellSelectEditor,
    ShadcnMarquee
]

interface InstallOptions
{
    locale?: Language
}

const install = (Vue: App, options: InstallOptions = {}) => {
    // 设置语言
    // Set language
    if (options.locale) {
        setLocale(options.locale)
    }

    components.map((component: any) => {
        Vue.component(component.__name as string, component)
    })

    // Support global import
    Vue.config.globalProperties.$Message = Message
    Vue.config.globalProperties.$Spin = Spin
    Vue.config.globalProperties.$setLocale = setLocale
}

let windowObj = window as any

if (typeof windowObj !== 'undefined' && windowObj.Vue) {
    const vm = windowObj.Vue.createApp({})
    install(vm)
}

// Auto import css
if (typeof window !== 'undefined') {
    import('../dist/view-shadcn-ui.min.css')
}

// Support on-demand import
export { default as ShadcnButton } from '@/ui/button'
export { default as ShadcnButtonGroup } from '@/ui/button/group'
export { default as ShadcnCard } from '@/ui/card'
export { ShadcnCopy } from '@/ui/copy'
export { ShadcnCodeEditor } from '@/ui/code-editor'
export { ShadcnInput } from '@/ui/input'
export { default as ShadcnIcon } from '@/ui/icon'
export { default as ShadcnModal } from '@/ui/modal'
export { default as ShadcnTooltip } from '@/ui/tooltip'
export { default as ShadcnRow } from '@/ui/row'
export { default as ShadcnCol } from '@/ui/col'
export { default as ShadcnDivider } from '@/ui/divider'
export { default as ShadcnEllipsis } from '@/ui/ellipsis'
export { default as ShadcnAvatar } from '@/ui/avatar'
export { default as ShadcnAvatarGroup } from '@/ui/avatar/group'
export { ShadcnBadge } from '@/ui/badge'
export { default as ShadcnAlert } from '@/ui/alert'
export { default as ShadcnProgress } from '@/ui/progress'
export { default as ShadcnSpace } from '@/ui/space'
export { default as ShadcnSwitch } from '@/ui/switch'
export { default as ShadcnRadio } from '@/ui/radio'
export { default as ShadcnRadioGroup } from '@/ui/radio/group'
export { default as ShadcnCheckbox } from '@/ui/checkbox'
export { default as ShadcnCheckboxGroup } from '@/ui/checkbox/group'
export { default as ShadcnSelect } from '@/ui/select'
export { default as ShadcnSelectOption } from '@/ui/select/option'
export { default as ShadcnSelectGroup } from '@/ui/select/group'
export { default as ShadcnRate } from '@/ui/rate'
export { ShadcnTab, ShadcnTabItem } from '@/ui/tab'
export { ShadcnSkeleton, ShadcnSkeletonItem } from '@/ui/skeleton'
export { default as ShadcnLayout } from '@/ui/layout'
export { default as ShadcnLayoutHeader } from '@/ui/layout/header'
export { default as ShadcnLayoutContent } from '@/ui/layout/content'
export { default as ShadcnLayoutSider } from '@/ui/layout/sider'
export { default as ShadcnLayoutFooter } from '@/ui/layout/footer'
export { default as ShadcnLayoutWrapper } from '@/ui/layout/wrapper'
export { default as ShadcnLayoutMain } from '@/ui/layout/main'
export { default as ShadcnPagination } from '@/ui/pagination'
export { default as ShadcnDrawer } from '@/ui/drawer'
export { default as ShadcnMenu } from '@/ui/menu'
export { default as ShadcnMenuItem } from '@/ui/menu/item'
export { default as ShadcnMenuSub } from '@/ui/menu/sub'
export { default as ShadcnMenuGroup } from '@/ui/menu/group'
export { default as ShadcnMessage } from '@/ui/message'
export { default as ShadcnTable } from '@/ui/table'
export { default as ShadcnBreadcrumb } from '@/ui/breadcrumb'
export { default as ShadcnBreadcrumbItem } from '@/ui/breadcrumb/item'
export { ShadcnSlider } from '@/ui/slider'
export { default as ShadcnSpin } from '@/ui/spin'
export { default as ShadcnForm } from '@/ui/form'
export { default as ShadcnFormItem } from '@/ui/form/item'
export { default as ShadcnTimeline } from '@/ui/timeline'
export { default as ShadcnTimelineItem } from '@/ui/timeline/item'
export { default as ShadcnTrend } from '@/ui/trend'
export { default as ShadcnException } from '@/ui/exception'
export { default as ShadcnLink } from '@/ui/link'
export { default as ShadcnCollapse } from '@/ui/collapse'
export { default as ShadcnCollapseItem } from '@/ui/collapse/item'
export { default as ShadcnTag } from '@/ui/tag'
export { default as ShadcnGlobalFooter } from '@/ui/footer/global'
export { default as ShadcnToolbarFooter } from '@/ui/footer/toolbar'
export { default as ShadcnWatermark } from '@/ui/watermark'
export { default as ShadcnHighlight } from '@/ui/highlight'
export { default as ShadcnDropdown } from '@/ui/dropdown'
export { default as ShadcnDropdownItem } from '@/ui/dropdown/item'
export { default as ShadcnNumber } from '@/ui/number'
export { default as ShadcnTree } from '@/ui/tree'
export { default as ShadcnText } from '@/ui/text'
export { default as ShadcnGradientText } from '@/ui/text/gradient'
export { ShadcnContextMenu, ShadcnContextMenuItem, ShadcnContextMenuSub } from '@/ui/contextmenu'
export { default as ShadcnHoverCard } from '@/ui/hover-card'
export { ShadcnToggle, ShadcnToggleGroup } from '@/ui/toggle'
export { ShadcnDataBuilderPanel, ShadcnDataBuilderCanvas, ShadcnDataBuilderConfigure, ShadcnDataBuilderEditor, ShadcnDataBuilderView } from '@/ui/data-builder'
export { ShadcnUpload } from '@/ui/upload'
export { ShadcnLogger } from '@/ui/logger'
export { ShadcnScrollbar } from '@/ui/scrollbar'
export { ShadcnCountDown } from '@/ui/count-down'
export { ShadcnEmpty } from '@/ui/empty'
export { ShadcnBackTop } from '@/ui/back-top'
export { ShadcnWorkflowEditor, ShadcnWorkflowView } from '@/ui/workflow'
export { ShadcnInputTag } from '@/ui/input-tag'
export { ShadcnMap } from '@/ui/map'
export { ShadcnLoadingBar } from '@/ui/loading-bar'
export { ShadcnFloatButton } from '@/ui/float-button'
export { ShadcnDataFilter, ShadcnHierarchicalDataFilter } from '@/ui/data-filter'
export { ShadcnQrCode } from '@/ui/qrcode'
export { ShadcnTimePicker } from '@/ui/time-picker'
export { ShadcnColorPicker } from '@/ui/color-picker'
export { ShadcnDatePicker } from '@/ui/date-picker'
export { ShadcnCron } from '@/ui/cron'
export { ShadcnContribution } from '@/ui/contribution'
export { ShadcnMention } from '@/ui/mention'
export { ShadcnImage, ShadcnImageGroup, ShadcnImageViewer } from '@/ui/image'
export { ShadcnCarousel } from '@/ui/carousel'
export { ShadcnDataTable, TableCellInputEditor, TableCellSelectEditor } from '@/ui/data-table'
export { ShadcnMarquee } from '@/ui/marquee'

// Export functions
export { fnToString, fnToFunction } from '@/utils/formatter'
export { formatLogger, formatFromExample, formatMultipleLines, formatLoggerFromStream } from '@/utils/logger'
export { randomUUID, validateUUID } from '@/utils/uuid'
export { getRecentTriggerTime } from '@/utils/cron'
export { LoadingBar } from '@/ui/loading-bar/service'

// Export locale
export { setLocale }

// Support global import
export default install

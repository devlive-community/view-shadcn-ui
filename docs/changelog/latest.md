---
title: 2025.1.2
---

# 🎉 View Shadcn UI v2025.1.2 发布公告：全新跑马灯组件与多项优化更新

<br />

亲爱的开发者们：

<br />

我们很高兴地宣布 View Shadcn UI 2025.1.2 版本正式发布！本次更新带来了全新的跑马灯组件，并对多个现有组件进行了功能增强和问题修复。

## 🚀 重要链接

- GitHub 仓库：https://github.com/devlive-community/view-shadcn-ui
- 官方文档：https://view-shadcn-ui.devlive.org/
- 在线演练场：https://playground.view-shadcn-ui.devlive.org/

## 📦 新版本亮点

### 🎪 全新跑马灯组件
- 灵活的滚动控制：
    - 支持自定义滚动速度
    - 鼠标悬停暂停功能
    - 可配置重复次数
- 简单易用的基础功能
- 流畅的动画效果

### ⌨️ 代码编辑器增强
- 多端点提示数据获取支持
- 优化自动提示词缓冲配置
- 新增提示服务访问限制
- 修复提示选择后的数据同步问题

### ✨ 现有组件优化
- 复选框：
    - 新增半选状态支持
    - 支持全选模式

- 功能修复：
    - 修复开关组件自定义文本溢出问题
    - 修复链接组件自动拉伸问题
    - 优化树组件懒加载节点的展开逻辑

## 🔜 后续规划

我们将继续专注于提升组件的易用性和性能，同时计划推出更多实用组件。欢迎社区参与贡献和提供反馈！

## 🤝 参与贡献

如果您在使用过程中遇到问题或有新的想法，欢迎：
- 提交 Issue：https://github.com/devlive-community/view-shadcn-ui/issues
- 贡献代码：https://github.com/devlive-community/view-shadcn-ui/pulls

感谢所有为这个版本贡献代码和反馈的开发者们！
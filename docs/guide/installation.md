# 安装

## 环境支持

所有支持 Vue3 的浏览器都支持 View Shadcn UI。

## 包管理器

建议使用包管理器（如 npm、yarn、pnpm 等）安装 View Shadcn UI。

对于打包工具，您可以选择自己喜欢的工具，例如 vite、webpack 等。

## 使用 npm

```bash
npm install -D view-shadcn-ui
```

## 使用 yarn

```bash
yarn add -D view-shadcn-ui
```

## 使用 pnpm

```bash
pnpm add -D view-shadcn-ui
```
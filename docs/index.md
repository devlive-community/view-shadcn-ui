---
layout: home

title: View Shadcn UI
titleTemplate: View Shadcn UI 是一个基于 Tailwind CSS 构建的 Vue3 组件库。

hero:
  name: View Shadcn UI
  tagline: |
    View Shadcn UI 是一个基于 Tailwind CSS 构建的 Vue3 组件库。 <br />
    <div class="flex gap-2">
        <img alt="NPM Downloads" src="https://img.shields.io/npm/d18m/view-shadcn-ui?style=flat&label=D">
        <img alt="GitHub stars" src="https://img.shields.io/github/stars/devlive-community/view-shadcn-ui?style=flat"/>
        <img alt="Gitee stars" src="https://gitee.com/devlive-community/view-shadcn-ui/badge/star.svg?theme=white"/>
    </div>
  image:
    alt: View Shadcn UI
    src: /logo.svg
  actions:
    - theme: brand
      text: 指南
      link: /guide/installation
    - theme: alt
      text: 在 GitHub 上查看
      link: https://github.com/devlive-community/view-shadcn-ui
    - theme: alt
      text: 在 Gitee 上查看
      link: https://gitee.com/devlive-community/view-shadcn-ui
    - theme: alt
      text: 在 NPM 上查看
      link: https://www.npmjs.com/package/view-shadcn-ui

features:
  - icon: 💡
    title: 组件美化
    details: 基于 Shadcn UI 和 Tailwin CSS 开发，美化各种底层组件
  - icon: 📦
    title: 开发工具
    details: 使用流行的 TypeScript 语言和 Vite 构建工具进行源代码和构建
  - icon: 🛠️
    title: 多种引入方式
    details: 支持全局 |按需引入组件，方便快捷的开发测试
  - icon: ⚙️
    title: 高度可定制性
    details: 提供灵活的配置选项，轻松自定义组件样式和功能
  - icon: 🚀
    title: 高性能
    details: 使用现代框架和优化的构建工具构建，以确保快速加载和流畅交互
  - icon: 🖥️
    title: 全面的文档
    details: 清晰详细的文档，带有示例，便于快速理解
  - icon: 🌐
    title: 多语言支持
    details: 支持多语言定制，以满足来自不同地区的用户
  - icon: 🔒
    title: 安全稳定
    details: 定期进行安全更新和主动维护，确保安全稳定使用
  - icon: 🎨
    title: 可主题化
    details: 在不同主题之间轻松切换，以满足您的品牌或项目的设计要求
  - icon: 📱
    title: 移动优先设计
    details: 响应式和移动友好型组件，可在任何设备上提供出色的体验
  - icon: 🧩
    title: 模块化结构
    details: 高度模块化的组件，允许在您的项目中灵活集成和扩展
  - icon: 📊
    title: 数据可视化
    details: 提供内置的数据可视化组件，用于创建交互式和富有洞察力的图表
  - icon: 🧑‍💻
    title: 开发人员友好的 API
    details: 直观且结构良好的 API，可加快开发过程并提高生产力
  - icon: 🧩
    title: 插件系统
    details: 可扩展的插件系统，用于与第三方工具集成并增强功能
  - icon: 📝
    title: 代码示例
    details: 即用型代码示例，帮助开发人员快速学习和集成组件
---
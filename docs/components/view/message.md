---
title: 消息 (Message)
---

# 介绍

<br />

本文档主要用于描述 `ShadcnMessage` 组件的一些特性和用法。

## 用法

<CodeRunner title="用法">
    <ShadcnButton @click="info(false)">Show Message</ShadcnButton>
</CodeRunner>

::: details 查看代码

```vue
<template>
    <ShadcnButton @click="handleClick">Show Message</ShadcnButton>
</template>

<script lang="ts">
import { defineComponent } from 'vue'

export default defineComponent({
  methods: {
    handleClick() {
      this.$Message.info({
        content: 'This is an info message',
        duration: 2
      })
    }
  }
})
```

:::

## 显示图标 (show-icon)

<CodeRunner title="显示图标 (show-icon)">
    <ShadcnButton @click="info(false)">Show Message</ShadcnButton>
</CodeRunner>

::: details 查看代码

```vue
<template>
    <ShadcnButton @click="handleClick">Show Message</ShadcnButton>
</template>

<script lang="ts">
import { defineComponent } from 'vue'

export default defineComponent({
  methods: {
    handleClick() {
      this.$Message.info({
        content: 'This is an info message',
        showIcon: true
      })
    }
  }
})
```

:::

## 类型 (type)

<CodeRunner title="类型 (type)">
  <ShadcnSpace wrap>
    <ShadcnButton @click="info(false)">Info</ShadcnButton>
    <ShadcnButton @click="success(false)">Success</ShadcnButton>
    <ShadcnButton @click="warning(false)">Warning</ShadcnButton>
    <ShadcnButton @click="error(false)">Error</ShadcnButton>
    <ShadcnButton @click="loading(false)">Loading</ShadcnButton>
  </ShadcnSpace>
</CodeRunner>

::: details 查看代码

```vue
<template>
  <ShadcnSpace wrap>
    <ShadcnButton @click="info">Info</ShadcnButton>
    <ShadcnButton @click="success">Success</ShadcnButton>
    <ShadcnButton @click="warning">Warning</ShadcnButton>
    <ShadcnButton @click="error">Error</ShadcnButton>
    <ShadcnButton @click="loading">Loading</ShadcnButton>
  </ShadcnSpace>
</template>

<script>
export default {
  methods: {
    info() {
      this.$Message.info({
        content: 'This is an info tip',
        type: 'info',
        showIcon: true
      });
    },
    success() {
      this.$Message.success({
        content: 'This is a success tip',
        type: 'success',
        showIcon: true
      });
    },
    warning() {
      this.$Message.warning({
        content: 'This is a warning tip',
        type: 'warning',
        showIcon: true
      });
    },
    error() {
      this.$Message.error({
        content: 'This is an error tip',
        type: 'error',
        showIcon: true
      });
    },
    loading() {
      this.$Message.loading({
        content: 'This is a loading tip',
        type: 'loading',
        showIcon: true
      });
    }
  }
}
</script>
```

:::

## 背景色 (background)

<CodeRunner title="背景色 (background)">
  <ShadcnSpace wrap>
    <ShadcnButton @click="info(true)">Info</ShadcnButton>
    <ShadcnButton @click="success(true)">Success</ShadcnButton>
    <ShadcnButton @click="warning(true)">Warning</ShadcnButton>
    <ShadcnButton @click="error(true)">Error</ShadcnButton>
    <ShadcnButton @click="loading(true)">Loading</ShadcnButton>
  </ShadcnSpace>
</CodeRunner>

::: details 查看代码

```vue
<template>
  <ShadcnSpace wrap>
    <ShadcnButton @click="info">Info</ShadcnButton>
    <ShadcnButton @click="success">Success</ShadcnButton>
    <ShadcnButton @click="warning">Warning</ShadcnButton>
    <ShadcnButton @click="error">Error</ShadcnButton>
    <ShadcnButton @click="loading">Loading</ShadcnButton>
  </ShadcnSpace>
</template>

<script>
export default {
  methods: {
    info() {
      this.$Message.info({
        content: 'This is an info tip',
        type: 'info',
        showIcon: true,
        background: true
      });
    },
    success() {
      this.$Message.success({
        content: 'This is a success tip',
        type: 'success',
        showIcon: true,
        background: true
      });
    },
    warning() {
      this.$Message.warning({
        content: 'This is a warning tip',
        type: 'warning',
        showIcon: true,
        background: true
      });
    },
    error() {
      this.$Message.error({
        content: 'This is an error tip',
        type: 'error',
        showIcon: true,
        background: true
      });
    },
    loading() {
      this.$Message.loading({
        content: 'This is a loading tip',
        type: 'loading',
        showIcon: true,
        background: true
      });
    }
  }
}
</script>
```

:::

## 可关闭 (closable)

<CodeRunner title="可关闭 (closable)">
    <ShadcnButton @click="closable">Closable</ShadcnButton>
</CodeRunner>

::: details 查看代码

```vue
<template>
    <ShadcnButton @click="closable">Show Message</ShadcnButton>
</template>

<script>
  export default {
    methods: {
      closable() {
        this.$Message.info({
          content: 'This is an info tip',
          type: 'info',
          showIcon: true,
          closable: true
        })
      }
    }
  }
</script>
```

:::

## 消息 (Message) 属性

<ApiTable title="消息 (Message) 属性"
    :headers="['属性', '描述', '类型', '默认值', '依赖']"
    :columns="[
            ['content', '内容文本', 'string', '-', '-'],
            ['duration', '消息的持续时间（以秒为单位），如果值为 0，则不会关闭消息', 'number', '1.5', '-'],
            ['showIcon', '是否显示图标', 'boolean', 'true', '-'],
            ['type', '组件的类型', 'string', 'info', 'info | success | warning | error | loading'],
            ['background', '是否显示背景，仅在设置类型时有效', 'boolean', 'false', '-'],
            ['closeable', '是否显示关闭按钮', 'boolean', 'false', '-'],
    ]">
</ApiTable>

## 消息 (Message) 插槽

<ApiTable title="消息 (Message) 插槽"
    :headers="['插槽', '描述']" 
    :columns="[
        ['default', '内容文本'],
        ['close', '关闭按钮'],
    ]">
</ApiTable>

## 消息 (Message) 事件

<ApiTable title="消息 (Message) 事件"
    :headers="['事件', '描述', '回调参数']"
    :columns="[
        ['on-close', '关闭消息时触发', 'event'],
    ]">
</ApiTable>

<script>
export default {
  methods: {
    info(background = false) {
      this.$Message.info({
        content: 'This is an info tip',
        type: 'info',
        showIcon: true,
        background: background
      });
    },
    success(background = false) {
      this.$Message.success({
        content: 'This is a success tip',
        type: 'success',
        showIcon: true,
        background: background
      })
    },
    warning(background = false) {
      this.$Message.warning({
        content: 'This is a warning tip',
        type: 'warning',
        showIcon: true,
        background: background
      })
    },
    error(background = false) {
      this.$Message.error({
        content: 'This is an error tip',
        type: 'error',
        showIcon: true,
        background: background
      })
    },
    loading(background = false) {
      this.$Message.loading({
        content: 'This is a loading tip',
        type: 'loading',
        showIcon: true,
        background: background
      })
    },
    closable() {
        this.$Message.info({
            content: 'This is an info tip',
            type: 'info',
            showIcon: true,
            duration: 0,
            closable: true
        });
      }
  }
}
</script>